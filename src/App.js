import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-toastify/dist/ReactToastify.css";

import Home from './pages/Home';
import Navbar from './layout/Navbar';
import Notfound from './pages/Notfound';
import AddUser from './pages/users/AddUser';
import EditUser from './pages/users/EditUser';


function App() {
  return (
    <Router>
      <div className="App">
      <Navbar/>
        <Switch>
            
            <Route exact path = '/' component={Home}></Route>
            <Route exact path = '/users/add' component={AddUser}></Route>
            <Route exact path = '/users/edit/:id' component={EditUser}></Route>
        
            <Route component={Notfound}></Route>

        </Switch>
      </div>
    </Router>
  );
}

export default App;
