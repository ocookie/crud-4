import React, {useState,useEffect} from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import {Zoom} from "react-toastify";


const Home = () => {
const[users,setUser] = useState([]);
// const [modalShow, setModalShow] = useState(false);

useEffect(() => {
loadUsers();
}, []);

const loadUsers = async () => {
const result = await axios.get("http://localhost:3003/users");
setUser(result.data);
};

const deleteUser = async id => {
    await axios.delete(`http://localhost:3003/users/${id}`)
    .then((response)=>{
        toast.warn("The user has been deleted.");
    })
    loadUsers();
}

// const ModalShow = () => {
//     setModalShow(true);
//     console.log("Modal state = ", modalShow)
// }


toast.configure({
    position: "top-center",
    autoClose: 3000,
    transition: Zoom,
  });

return(
<div className="container shadow">
    <div className="py-2">
        <h1>Homepage</h1>
        {/* {modalShow && <Modal/>} */}
        <table className="table table-striped">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Username</th>
                    <th scope="col">Email</th>
                    <th scope="col">Website</th>
                    <th scope="col">Contact</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {
                    users.map((user,index) => (
                        <tr>
                            <th scope="row">{index+1}</th>
                            <td>{user.name}</td>
                            <td>{user.username}</td>
                            <td>{user.email}</td>
                            <td>{user.website}</td>
                            <td>{user.phone}</td>
                            <td>
                            {/* <Link type="button" className="btn btn-danger" onClick={ModalShow} > Delete </Link> */}
                            {/* <Link type="button" className="btn btn-danger> Delete </Link> */}
                            <Link to={`#`} type="button" className="btn btn-danger" 
                            onClick={ () => {
                                if
                                (window.confirm
                                    ('You are about to delete a record.')
                                )
                                {
                                        deleteUser(user.id)};
                                }
                                    } >Delete 
                            </Link>
                            <Link type="button" className="btn btn-default" to={`users/edit/${user.id}`}>Edit</Link>
                            </td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    </div>
</div>
)
}

export default Home;



// Code to ask for confirmation of deletion in alert tab.
// <Link to={`#`} type="button" className="btn btn-danger" 
//                             onClick={ () => {
//                                 if
//                                 (window.confirm
//                                     ('Are you sure to delete this record')
//                                 )
//                                 {
//                                         deleteUser(user.id)};
//                                 }
//                                     } >Delete 
//                             </Link>